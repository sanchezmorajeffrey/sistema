<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Proveedor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProveedorController extends Controller
{

    public function index(Request $request)
    {
        //
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        if(!$request->ajax()) return redirect('/');

        if ($buscar=='') {
            # code...
            $persona = Proveedor::join('personas','proveedores.id','=','personas.id')
            ->select('personas.id','personas.nombre','personas.tipo_documento',
            'personas.num_documento','personas.direccion','personas.telefono','personas.email',
            'proveedores.contacto','proveedores.telefono_contacto')
            ->orderBy('personas.id','desc')->paginate(4);
        } else {
            # code...
            $persona = Proveedor::join('personas','proveedores.id','=','personas.id')
            ->select('personas.id','personas.nombre','personas.tipo_documento',
            'num_documento','personas.direccion','personas.telefono','personas.email',
            'proveedores.contacto','proveedores.telefono_contacto')
            ->where('personas.'.$criterio , 'like','%'.$buscar.'%')
            ->orderBy('personas.id','desc')->paginate(4);
        }
        return [
            'pagination' => [
                'total' => $persona->total(),
                'current_page' => $persona->currentPage(),
                'per_page' => $persona->perPage(),
                'last_page' => $persona->lastPage(),
                'from' => $persona->firstItem(),
                'to' => $persona ->lastItem(),
            ],
            'persona' => $persona
        ];
        
    }

    public function selectProveedor(Request $request){
        //if(!$request->ajax()) return redirect('/');
        $filtro = $request->filtro;

        $proveedores = Proveedor::join('personas','proveedores.id','=','personas.id')
        ->where('personas.nombre','like','%'.$filtro.'%')
        ->orWhere('personas.num_documento','like','%'.$filtro.'%')
        ->select('personas.id','personas.nombre','personas.num_documento')
        ->orderBy('personas.nombre','asc')->get();

        return  ['proveedores' => $proveedores];
        
    }

    public function store(Request $request)
    {
        //
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction(); 
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email= $request->email;

            $persona->save();

            $proveedor = new Proveedor();
            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->id = $persona->id;

            $proveedor ->save();

            DB::commit();

        }catch(Exception $e) {
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
        //
        try{
            DB::beginTransaction(); 

            //buscar el primer proveedor 
            $proveedor = Proveedor::findOrFail($request->id);

            $persona = Persona::findOrFail($proveedor->id);
            
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email= $request->email;

            $persona->save();


            $proveedor->contacto = $request->contacto;
            $proveedor->telefono_contacto = $request->telefono_contacto;
            $proveedor->id = $persona->id;

            $proveedor ->save();

            DB::commit();

        }catch(Exception $e) {
            DB::rollBack();
        }
    }


}
