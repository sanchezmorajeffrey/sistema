<?php

namespace App\Http\Controllers;

use App\Rol;
use App\Persona;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        //
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        //if(!$request->ajax()) return redirect('/');

        if ($buscar=='') {
            # code...
            $persona = User::join('personas', 'users.id','=','personas.id')
            ->join('roles', 'users.idrol','=', 'roles.id')
            ->select('personas.id', 'personas.nombre', 'personas.tipo_documento', 'personas.num_documento',
            'personas.direccion', 'personas.telefono','personas.email', 'users.usuario', 'users.password',
            'users.condicion','users.idrol','roles.nombre as rol')
            ->orderBy('personas.id', 'desc')->paginate(4);
        } else {
            # code...
            $persona = User::join('personas', 'users.id','=','personas.id')
            ->join('roles', 'users.idrol','=', 'roles.id')
            ->select('personas.id', 'personas.nombre', 'personas.tipo_documento', 'personas.num_documento',
            'personas.direccion', 'personas.telefono','personas.email', 'users.usuario', 'users.password',
            'users.condicion','users.idrol','roles.nombre as rol')
            ->where('personas.'.$criterio, 'like', '%'.$buscar.'%')
            ->orderBy('personas.id', 'desc')->paginate(4);
        }

        return  [
            'pagination' => [
                'total' => $persona->total(),
                'current_page' => $persona->currentPage(),
                'per_page' => $persona->perPage(),
                'last_page' => $persona->lastPage(),
                'from' => $persona->firstItem(),
                'to' => $persona ->lastItem(),
            ],
            'persona' => $persona
        ];
        
    }
    public function store(Request $request)
    {
        //
        if(!$request->ajax()) return redirect('/');

        try{
            DB::beginTransaction();
            $persona = new Persona();
            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;

            $persona->save();

            $user = new User();
            $user->usuario = $request->usuario;
            $user->password = bcrypt($request->password);
            $user->condicion = '1';
            $user->idrol = $request->idrol;

            $user->id = $persona->id;

            $user->save();

            DB::commit();

        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
        //
        
        if(!$request->ajax()) return redirect('/');

        try{

            DB::beginTransaction();

            //buscar el primer usuario
            $user = User::findOrFail($request->id);

            $persona = Persona::findOrFail($user->id);

            $persona->nombre = $request->nombre;
            $persona->tipo_documento = $request->tipo_documento;
            $persona->num_documento = $request->num_documento;
            $persona->direccion = $request->direccion;
            $persona->telefono = $request->telefono;
            $persona->email = $request->email;

            $persona->save();


            $user->usuario = $request->usuario;

            if($request->password != null ){
                $user->password = bcrypt($request->password);
            }
            else{
                $request->password = $user->password;
            }
                                    
            $user->condicion = '1';
            $user->idrol = $request->idrol;
            $user->save();

            DB::commit();

        }catch(Exception $e){
            DB::rollBack();
        }
    }

    public function desactivar(Request $request){
        //
        if(!$request->ajax()) return redirect('/');

        $user = User::findOrFail($request->id); 
        $user->condicion='0';
        $user->save();
    }

    public function activar(Request $request){
        //
        if(!$request->ajax()) return redirect('/');

        $user = User::findOrFail($request->id); 
        $user->condicion='1';
        $user->save();

    }

}
