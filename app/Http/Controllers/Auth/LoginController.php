<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;


class LoginController extends Controller
{

    public function showLoginForm(){
        return view('auth.login');
    }
    public function login(Request $request){
        $this->validateLogin($request);
        //$this->hasTooManyLoginAttempts($request);
        //$this->hasTooManyLoginAttempts($request);
        if (Auth::attempt(['usuario' => $request->usuario, 'password' => $request->password, 'condicion'=>1])) {
            # code...
            return redirect()->route('main'); 
        }
        return back()
        ->withErrors(['usuario' => trans('auth.failed')])
        ->withInput(request(['usuario']));
    }

    protected function validateLogin(Request $request){
        //validar el inicio 
        $this->validate($request,[
            'usuario'  => 'required|string',
            'password' => 'required|string'
        ]);
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 1, 1
        );      
    }

}
