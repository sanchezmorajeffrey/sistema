<?php

namespace App\Http\Controllers;

use App\Rol;
use Illuminate\Http\Request;

class RolController extends Controller
{

    public function index(Request $request)
    {
        //
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        //if(!$request->ajax()) return redirect('/');

        if ($buscar=='') {
            # code...
            $rol = Rol::orderBy('id','desc')->paginate(4);
        } else {
            # code...
            $rol = Rol::where($criterio , 'like','%'.$buscar.'%')->orderBy('id', 'desc')->paginate(4);
        }
        return [
            'pagination' => [
                'total' => $rol->total(),
                'current_page' => $rol->currentPage(),
                'per_page' => $rol->perPage(),
                'last_page' => $rol->lastPage(),
                'from' => $rol->firstItem(),
                'to' => $rol ->lastItem(),
            ],
            'rol' => $rol
        ];
        
    }

    public function selectRol(Request $request){
        //if(!$request->ajax()) return redirect('/');
        $rol = Rol::where('condicion','=','1')
        ->select('id','nombre')
        ->orderBy('nombre','asc')->get();
        return ['rol' => $rol];
    }
}
