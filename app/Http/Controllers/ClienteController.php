<?php

namespace App\Http\Controllers;

use App\Persona;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(Request $request)
    {
        //
        //if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar=='') {
            # code...
            $persona = Persona::orderBy('id','desc')->paginate(4);
        } else {
            # code...
            $persona = Persona::where($criterio ,'like', '%'.$buscar.'%')->orderBy('id','desc')->paginate(4);
        }
        return [
            'pagination' => [
                'total' => $persona->total(),
                'current_page' => $persona->currentPage(),
                'per_page' => $persona->perPage(),
                'last_page' => $persona->lastPage(),
                'from' => $persona->firstItem(),
                'to' => $persona ->lastItem(),
            ],
            'personas' => $persona  
        ];
        
    }

    public function selectCliente(Request $request)
    {
        if(!$request->ajax()) redirect('/');

        $filtro = $request->filtro;

        $clientes = Persona::where('nombre','like','%'.$filtro.'%')
        ->orWhere('num_documento','like','%'.$filtro.'%')
        ->select('id','nombre','num_documento')
        ->orderBy('nombre','desc')->get();

        return ['clientes' => $clientes];

    }

    public function store(Request $request)
    {
        //
        if(!$request->ajax()) return redirect('/');

        $persona = new Persona();
        $persona->nombre = $request->nombre;
        $persona->tipo_documento = $request->tipo_documento;
        $persona->num_documento = $request->num_documento;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;

        $persona->save();
    }

    public function update(Request $request)
    {
        //
        if(!$request->ajax()) return redirect('/');
        $persona = Persona::findOrFail($request->id);
        $persona->nombre = $request->nombre;
        $persona->tipo_documento = $request->tipo_documento;
        $persona->num_documento = $request->num_documento;
        $persona->direccion = $request->direccion;
        $persona->telefono = $request->telefono;
        $persona->email = $request->email;

        $persona->save();

    }
}
