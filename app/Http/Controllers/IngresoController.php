<?php

namespace App\Http\Controllers;

use App\Ingreso;
use App\DetalleIngreso;
use App\User;
use App\Notifications\NotifyAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class IngresoController extends Controller
{

    public function index(Request $request)
    {
        //
        $buscar = $request->buscar;
        $criterio = $request->criterio;
        //if(!$request->ajax()) return redirect('/');

        if ($buscar=='') {
            # code...
            $ingresos = Ingreso::join('personas','ingresos.idproveedor','=','personas.id')
            ->join('users','ingresos.idusuario','=','users.id')
            ->select('ingresos.id','ingresos.tipo_comprobante','ingresos.serie_comprobante',
            'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total',
            'ingresos.estado', 'personas.nombre', 'users.usuario')
            ->orderBy('ingresos.id','desc')->paginate(4);
        } else {
            # code...
            $ingresos = Ingreso::join('personas','ingresos.idproveedor','=','personas.id')
            ->join('users','ingresos.idusuario','=','users.id')
            ->select('ingresos.id','ingresos.tipo_comprobante','ingresos.serie_comprobante',
            'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total',
            'ingresos.estado', 'personas.nombre', 'users.usuario')
            ->where('ingresos.'.$criterio, 'like', '%'. $buscar .'%')
            ->orderBy('ingresos.id','desc')->paginate(4);
        }
        return [
            'pagination' => [
                'total' => $ingresos->total(),
                'current_page' => $ingresos->currentPage(),
                'per_page' => $ingresos->perPage(),
                'last_page' => $ingresos->lastPage(),
                'from' => $ingresos->firstItem(),
                'to' => $ingresos ->lastItem(),
            ],
            'ingresos' => $ingresos
        ];
        
    }
    public function obtenerCabecera(Request $request){
        if(!$request->ajax()) return redirect("/");

        $id = $request->id;

        $ingreso = Ingreso::join('personas','ingresos.idproveedor','=','personas.id')
        ->join('users','ingresos.idusuario','=','users.id')
        ->select('ingresos.id','ingresos.tipo_comprobante','ingresos.serie_comprobante',
        'ingresos.num_comprobante','ingresos.fecha_hora','ingresos.impuesto','ingresos.total',
        'ingresos.estado', 'personas.nombre', 'users.usuario')
        ->where('ingresos.id','=',$id)
        ->orderBy('ingresos.id','desc')->take(1)->get();
            
        return ['ingreso' => $ingreso];
    }
    public function obtenerDetalle(Request $request){
        
        if(!$request->ajax()) return redirect("/");

        $id = $request->id;

        $detalles = DetalleIngreso::join('articulos','detalle_ingresos.idarticulo','=','articulos.id')
        ->select('detalle_ingresos.cantidad','detalle_ingresos.precio','articulos.nombre as articulo')
        ->where('detalle_ingresos.idingreso','=',$id)
        ->orderBy('detalle_ingresos.id','desc')->get();
            
        return ['detalles' => $detalles];
    }
    public function store(Request $request)
    {
        //
        if(!$request->ajax()) return redirect('/');
        try{
            DB::beginTransaction();

            $mytime = \Carbon\Carbon::now('America/Managua');

            $ingresos = new Ingreso();

            $ingresos->idproveedor = $request->idproveedor;
            $ingresos->idusuario   = \Auth::user()->id;
            $ingresos->tipo_comprobante = $request->tipo_comprobante;
            $ingresos->serie_comprobante = $request->serie_comprobante;
            $ingresos->num_comprobante = $request->num_comprobante;
            $ingresos->fecha_hora = $mytime->toDateTimeString();
            $ingresos->impuesto = $request->impuesto;
            $ingresos->total = $request->total;
            $ingresos->estado = "Registrado";

            $ingresos->save();

            $detalles = $request->data; //Array de ingresos

            foreach ($detalles as $ep => $det) {
                # code...
                $detalle = new DetalleIngreso();
                $detalle->idingreso = $ingresos->id;
                $detalle->idarticulo = $det["idarticulo"];
                $detalle->cantidad = $det["cantidad"];
                $detalle->precio = $det["precio"];

                $detalle->save();
            }

            $fechaActual = Date('Y-m-d');
            $numVentas = DB::table('ventas')->whereDate('created_at', $fechaActual)->count();
            $numIngresos = DB::table('ingresos')->whereDate('created_at', $fechaActual)->count();

            $arreglosDatos = [
                'ventas' => [
                    'numero' => $numVentas,
                    'msj'    => 'Ventas'
                ],
                'ingresos' => [
                    'numero' => $numIngresos,
                    'msj'    => 'Ingresos'
                ]
            ];
            $allUser = User::all();
            foreach ($allUser as $notificar){
                User::findOrFail($notificar->id)->notify(new NotifyAdmin($arreglosDatos));
            }

            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
        }
    }
    public function desactivar(Request $request){
        if(!$request->ajax()) return redirect('/');

        $ingresos = Ingreso::findOrFail($request->id);
        $ingresos->estado = "Anulado";
        $ingresos->save();
    }
}
