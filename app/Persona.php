<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    protected $fillable = [
        'nombre','tipo_documento','num_documento','direccion','telefono','email'
    ];

    public function Proveedor(){
        return $this->hasOne('App\Proveedor');
    }

    public function User(){
        return $this->hasOne('App\User');
    }
}
